package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Client;
import cz.vse.hotelsemestral.repository.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService implements IClientService {

    @Autowired
    ClientRepo clientRepo;

    @Override
    public List<Client> getAllClients() {
        return clientRepo.findAll();
    }

    @Override
    public void deleteClient(int id) {
        clientRepo.deleteById(id);
    }

    @Override
    public void updateClient(Client client) {
        clientRepo.saveAndFlush(client);
    }

    @Override
    public void createClient(Client client) {
        clientRepo.saveAndFlush(client);
    }

    @Override
    public Client getClientByEmail(String email) {
        return clientRepo.getClientByEmail(email);
    }

    @Override
    public Client findById(int id) {
        return clientRepo.findById(id);
    }
}
