package cz.vse.hotelsemestral.service;

import cz.vse.hotelsemestral.entity.Room;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface IRoomService {

    void createRoom(final Room room);

    Room getRoomByName (String name);

    List <Room> getAvailableRooms (Date checkIn, Date checkOut);

}
